import React from 'react';
import PropTypes from 'prop-types';

const Card = ({ title, image }) => (
  <section className="cards__item">
    <div className="card">
      <div className={`card__image image__${image}`} />
      <div className="card__content">
        <article>
          <h2 className="card__title">{title}</h2>
          <p className="card__text">
            Eiiitaaa Mainhaaa!! Esse Lorem ipsum é só na sacanageeem!!
            E que abundância meu irmão viuu!! Assim você vai matar o papai.
            Só digo uma coisa, Domingo ela não vai! Danadaa!!
            Vem minha odalisca, agora faz essa cobra coral subir!!!
            Pau que nasce torto, Nunca se endireita. Tchannn!! Tchannn!! Tu du du pááá!
            Eu gostchu muitchu, heinn! danadinha! Mainhaa! Agora use meu lorem
            ipsum ordinária!!! Olha o quibeee! rema, rema, ordinária!.
          </p>
        </article>
      </div>
    </div>
  </section>
);

Card.propTypes = {
  title: PropTypes.string.isRequired,
  image: PropTypes.string.isRequired,
};

export default Card;
