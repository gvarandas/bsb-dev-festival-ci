import React from 'react';

const Footer = () => (
  <footer className="footer">
    <span>COPYRIGHT © 2018 - GUILHERME VARANDAS</span>
  </footer>
);

export default Footer;
