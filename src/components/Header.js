import React from 'react';
import logo from '../images/hangloose.png';

const Header = () => (
  <header className="header-container">
    <img src={logo} className="header-logo" alt="logo" />
    <h1 className="header-title">OLAR TÍTULO</h1>
  </header>
);

export default Header;
