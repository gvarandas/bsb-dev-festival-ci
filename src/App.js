import React from 'react';
import './App.css';

import Header from './components/Header';
import Card from './components/Card';
import Footer from './components/Footer';

const CARD_INFO = [
  { key: 1, title: 'Rogerinho do Ingá', image: 'rogerinho' },
  { key: 2, title: 'Caito Sensual', image: 'caito' },
  { key: 3, title: 'Cerginho na Praia', image: 'cerginho_sunga' },
  { key: 4, title: 'Bruno Aleixo', image: 'falha_bruno_aleixo' },
  { key: 5, title: 'Mini Hangloose', image: 'minihl' },
];

const App = () => (
  <div className="page-container">
    <Header />
    <main>
      <section className="cards">
        {CARD_INFO.map(({ key, title, image }) => <Card key={key} title={title} image={image} />)}
      </section>
    </main>
    <Footer />
  </div>
);

export default App;
