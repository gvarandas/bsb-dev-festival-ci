# OLAR BRASÍLIA DEV FESTIVAL

## Viram como é fácil montar um ambiente bem configurado?
Clonem esse projeto e sejam felizes <3

---

Clone o projeto

`git clone https://gitlab.com/gvarandas/bsb-dev-festival-ci.git`

Instale as dependências

`yarn` ou `npm install`

Execute o projeto em ambiente local

`npm start`

---

**Autor: Guilherme Varandas**

Material de Apoio para a apresentação

`"Mantendo a saúde do seu projeto JS (E a sanidade do seu time)"`